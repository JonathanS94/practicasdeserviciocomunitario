from typing import Optional, Tuple
import arcade
import pyglet
from PyQt5.QtWidgets import QPushButton


class MyFirstWindow(arcade.Window):
    
    def __init__(self, width: int = 600, height: int = 800, title: str = "Mi primer ventana"):   
        super().__init__(width, height, title)
        arcade.set_background_color(arcade.color.TEAL_BLUE)
        self.set_location(0,0)
        self._logoApp = arcade.Sprite("sprites/logoApp.png", scale = 0.75, center_x = 150, center_y=180)
        self._rectangle_posicion_x = 400
        self._rectangle_posicion_y = 200
         
    def on_draw(self):
        arcade.start_render()
        self._logoApp.draw()
        arcade.draw_rectangle_filled(self._rectangle_posicion_x,self._rectangle_posicion_y,400,200,arcade.color.AERO_BLUE)
        
        return super().on_draw()

    
    